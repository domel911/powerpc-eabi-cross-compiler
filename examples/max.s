###PROGRAM DATA###
.data
.align 3
#value_list is the address of the beginning of the list
value_list:
        .long 23, 50, 95, 96, 37, 85
#value_list_end is the address immediately after the list
value_list_end:

###ACTUAL CODE###
.text
    .global _start
_start:

        #REGISTER USE DOCUMENTATION
        #register 3 -- current maximum
        #register 4 -- current value address
        #register 5 -- stop value address
        #register 6 -- current value

        #load the address of value_list into register 4
        lis 4, value_list@ha
        addi 4, 4, value_list@l

        #load the address of value_list_end into register 5
        lis 5, value_list_end@ha
        addi 5, 5, value_list_end@l

        #initialize register 3 to 0
        li 3, 0

        #MAIN LOOP
loop:
        #compare register 4 to 5
        cmpw 4, 5
        #if equal branch to end
        beq end

        #load the next value
        lwz 6, 0(4)

        #compare register 6 (current value) to register 3 (current maximum)
        cmpw 6, 3
        #if reg. 6 is not greater than reg. 3 then branch to loop_end
        ble loop_end

        #otherwise, move register 6 (current) to register 3 (current max)
        mr 3, 6

loop_end:
        #advance pointer to next value (advances by 8-bytes)
        addi 4, 4, 8
        #go back to beginning of loop
        b loop


end:
        #set the system call number
        li 0, 1
        #register 3 already has the value to exit with
        #signal the system call
        sc
