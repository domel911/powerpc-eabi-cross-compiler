.text
    .global _start
_start:
#Immediate mode D-Form instructions have form: opcode dst, src, value

#Add the contents of register 3 to the number 25 and store in register 2
addi 2, 3, 25

#OR the contents of register 6 to the number 0b0000000000000001 and store in register 3
ori 3, 6, 0b00000000000001

#Move the number 55 into register 7
#(remember, when 0 is the second register in D-Form instructions
#it means ignore the register)
addi 7, 0, 55
#Here is the extended mnemonics for the same instruction
li 7, 55

#Load/store instructions have form: opcode dst, d(a)
#where d - numeric address/offset, a - register number for numeric address/offset

#load a byte from the address in register 2, store it in register 3, 
#and zero out the remaining bits
lbz 3, 0(2)

#store the 64-bit contents (double-word) of register 5 into the 
#address 32 bits past the address specified by register 23
#std 5, 32(23)

#store the low-order 32 bits (word) of register 5 into the address 
#32 bits past the address specified by register 23
stw 5, 32(23)

#store the byte in the low-order 8 bits of register 30 into the 
#address specified by register 4
stb 30, 0(4)

#load the 16 bits (half-word) at address 300 into register 4, and 
#zero-out the remaining bits
lhz 4, 300(0)

#load the half-word (16 bits) that is 1 byte offset from the address 
#in register 31 and store the result sign-extended into register 18
lha 18, 1(31)

#X-Form load store instruction form: opcode dst, rega, regb
#where rega and regb are the registers used for address calculation

#Load a doubleword (64 bits) from the address specified by 
#register 3 + register 20 and store the value into register 31
#ldx 31, 3, 20

#Load a byte from the address specified by register 10 + register 12 
#and store the value into register 15 and zero-out remaining bits
lbzx 15, 10, 12

#Load a halfword (16 bits) from the address specified by 
#register 6 + register 7 and store the value into register 8, 
#sign-extending the result through the remaining bits
lhax 8, 6, 7

#Take the doubleword (64 bits) in register 20 and store it in the 
#address specified by register 10 + register 11
#stdx 20, 10, 11

#Take the doubleword (64 bits) in register 20 and store it in the 
#address specified by register 10 + register 11, and then update 
#register 10 with the final address
#stdux 20, 10, 11
