#!/bin/bash

set -e
set -x

shopt -s extglob

SCRIPTDIR=`dirname $0`
SCRIPTDIR=`readlink -f $SCRIPTDIR`
PACKAGESDIR=$SCRIPTDIR/packages
UNCOMPRESSDIR=$SCRIPTDIR/uncompress

#export environment variables
export TARGET=powerpc-eabi
export PREFIX=$SCRIPTDIR/$TARGET
export PATH=$PATH:$PREFIX/bin

rm -rf $PACKAGESDIR
rm -rf $UNCOMPRESSDIR
rm -rf $PREFIX

#create environment file
echo "PATH=$PATH" >> "$TARGET"-env.sh
chmod a+x "$TARGET"-env.sh

#obtain necessary packages
mkdir -p $PACKAGESDIR
declare -a packages=( 
http://ftpmirror.gnu.org/binutils/binutils-2.24.tar.gz
http://ftpmirror.gnu.org/gcc/gcc-4.9.2/gcc-4.9.2.tar.gz
https://www.kernel.org/pub/linux/kernel/v3.x/linux-3.17.2.tar.xz
http://ftpmirror.gnu.org/glibc/glibc-2.20.tar.xz
http://ftpmirror.gnu.org/mpfr/mpfr-3.1.2.tar.xz
http://ftpmirror.gnu.org/gmp/gmp-6.0.0a.tar.xz
http://ftpmirror.gnu.org/mpc/mpc-1.0.2.tar.gz
ftp://gcc.gnu.org/pub/gcc/infrastructure/isl-0.12.2.tar.bz2
ftp://gcc.gnu.org/pub/gcc/infrastructure/cloog-0.18.1.tar.gz
ftp://sourceware.org/pub/newlib/newlib-2.2.0.20150423.tar.gz  
http://ftp.gnu.org/gnu/gdb/gdb-7.9.tar.gz
)

for i in "${packages[@]}"
do
    wget --directory-prefix=$PACKAGESDIR $i
done

#decompress obtained packages
mkdir -p $UNCOMPRESSDIR
for f in $PACKAGESDIR/*.tar*; do tar xf $f -C $UNCOMPRESSDIR; done

GCCDIR=$(ls -d $UNCOMPRESSDIR/gcc-*)
LINKDIRS=$(ls -d $UNCOMPRESSDIR/+(mpfr|gmp|mpc|isl|cloog)-*)

for d in $LINKDIRS ; do 
    LINKNAME=`basename $d`
    LINKNAME=${LINKNAME%%-*}
    LINKDIR=`readlink -f $d`

    ln -s $LINKDIR $GCCDIR/$LINKNAME
done

cd $UNCOMPRESSDIR

#build binutils
BINUTILSDIR=`ls -d binutils-*`
BINUTILSDIR=`readlink -f $BINUTILSDIR`

mkdir build-binutils
cd build-binutils
$BINUTILSDIR/configure --target=$TARGET --prefix=$PREFIX
make -j all
make install
cd ..

#buid bootstrap GCC
GCCDIR=`ls -d gcc-*`
GCCDIR=`readlink -f $GCCDIR`

mkdir build-gcc
cd build-gcc
$GCCDIR/configure --target=$TARGET --prefix=$PREFIX --without-headers --with-newlib  --with-gnu-as --with-gnu-ld
make -j all-gcc
make install-gcc
cd ..

#build newlib
NEWLIBDIR=`ls -d newlib-*`
NEWLIBDIR=`readlink -f $NEWLIBDIR`
mkdir build-newlib
cd build-newlib
$NEWLIBDIR/configure --target=$TARGET --prefix=$PREFIX
make -j all
make install
cd ..

#build GCC with newlib
cd build-gcc
$GCCDIR/configure --target=$TARGET --prefix=$PREFIX --with-newlib --with-gnu-as --with-gnu-ld --disable-shared --disable-libssp
make -j all
make install
cd ..

#build GDB with PSIM
GDBDIR=`ls -d gdb-*`
GDBDIR=`readlink -f $GDBDIR`

mkdir build-gdb
cd build-gdb
$GDBDIR/configure -target=$TARGET --prefix=$PREFIX --enable-sim-powerpc --enable-sim-stdio
make -j all
make install
